package au.com.bfbapps.pulselive.data

import au.com.bfbapps.pulselive.models.ContentDetail
import au.com.bfbapps.pulselive.models.ContentList
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface ContentService {

  @GET("test/native/contentList.json")
  fun getContentItemsList(): Observable<ContentList>

  @GET("test/native/content/{id}.json")
  fun getContentItem(@Path("id") id: Int): Observable<ContentDetail>

  companion object {
    fun create(): ContentService {
      val retrofit = Retrofit.Builder()
          .addConverterFactory(GsonConverterFactory.create())
          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
          .baseUrl("http://dynamic.pulselive.com/")
          .build()

      return retrofit.create(ContentService::class.java)
    }
  }

}