package au.com.bfbapps.pulselive.presenter

import au.com.bfbapps.pulselive.data.ContentService
import au.com.bfbapps.pulselive.view.MainActivityView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainActivityPresenter(contentService: ContentService, view: MainActivityView): BasePresenter<MainActivityView>(contentService, view) {

  fun getContentItems() {
    disposable = contentService.getContentItemsList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .map { it.contentItems }
        .subscribe { view.render(it) }
  }

}