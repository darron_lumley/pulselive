package au.com.bfbapps.pulselive.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.bfbapps.pulselive.R


class LoadingItemDelegateAdapter: ViewTypeDelegateAdapter {
  override fun onCreateViewHolder(parent: ViewGroup) = LoadingViewHolder(parent)

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
  }

  class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
      LayoutInflater.from(parent.context).inflate(R.layout.content_item_loading, parent, false))
}