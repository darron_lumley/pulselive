package au.com.bfbapps.pulselive.view.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import au.com.bfbapps.pulselive.models.ContentItem


class ContentAdapter(listener: OnItemClickedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

  private var items: ArrayList<ViewType>
  private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
  private val loadingItem = object : ViewType {
    override fun getViewType() = AdapterItems.LOADING
  }

  init {
    delegateAdapters.put(AdapterItems.LOADING, LoadingItemDelegateAdapter())
    delegateAdapters.put(AdapterItems.CONTENT, ContentItemDelegateAdapter(listener))
    items = ArrayList()
    items.add(loadingItem)
  }

  override fun getItemCount() = items.size

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
      delegateAdapters.get(viewType).onCreateViewHolder(parent)

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])
  }

  override fun getItemViewType(position: Int) = items[position].getViewType()

  fun clearAndAddContentItems(contentItems: List<ContentItem>) {
    items.clear()
    items.addAll(contentItems)
    notifyDataSetChanged()
  }

}