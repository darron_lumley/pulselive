package au.com.bfbapps.pulselive.view.adapter


object AdapterItems {
  val CONTENT = 1
  val LOADING = 2
}