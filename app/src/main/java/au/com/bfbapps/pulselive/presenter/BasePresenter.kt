package au.com.bfbapps.pulselive.presenter

import au.com.bfbapps.pulselive.data.ContentService
import io.reactivex.disposables.Disposable


abstract class BasePresenter<T>(val contentService: ContentService, val view: T) {

  var disposable: Disposable? = null

  fun pause() {
    disposable?.dispose()
  }
}