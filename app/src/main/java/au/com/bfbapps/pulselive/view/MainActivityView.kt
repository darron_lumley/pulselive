package au.com.bfbapps.pulselive.view

import au.com.bfbapps.pulselive.models.ContentItem


interface MainActivityView {
  fun render(contentItems: List<ContentItem>)
}