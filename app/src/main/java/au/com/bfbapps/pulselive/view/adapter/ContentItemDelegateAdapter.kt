package au.com.bfbapps.pulselive.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import au.com.bfbapps.pulselive.models.ContentItem


class ContentItemDelegateAdapter(private val onItemClickedListener: OnItemClickedListener) : ViewTypeDelegateAdapter  {

  override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
    return ContentItemViewHolder(parent)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    holder as ContentItemViewHolder
    holder.bind(item as ContentItem, onItemClickedListener)
  }

}