package au.com.bfbapps.pulselive.view.adapter

import android.view.View
import au.com.bfbapps.pulselive.models.ContentItem


interface OnItemClickedListener {
  fun onItemClicked(contentItem: ContentItem, titleView: View, subtitleView: View, dateView: View)
}