package au.com.bfbapps.pulselive.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import au.com.bfbapps.pulselive.R
import au.com.bfbapps.pulselive.data.ContentService
import au.com.bfbapps.pulselive.models.ContentItem
import au.com.bfbapps.pulselive.presenter.MainActivityPresenter
import au.com.bfbapps.pulselive.view.ContentDetailActivity.Companion.EXTRA_CONTENT_ITEM
import au.com.bfbapps.pulselive.view.ContentDetailActivity.Companion.TRANSITION_DATE
import au.com.bfbapps.pulselive.view.ContentDetailActivity.Companion.TRANSITION_SUBTITLE
import au.com.bfbapps.pulselive.view.ContentDetailActivity.Companion.TRANSITION_TITLE
import au.com.bfbapps.pulselive.view.adapter.ContentAdapter
import au.com.bfbapps.pulselive.view.adapter.OnItemClickedListener


class MainActivity : AppCompatActivity(), MainActivityView, OnItemClickedListener {

  private val presenter: MainActivityPresenter by lazy { MainActivityPresenter(ContentService.create(), this) }
  private val recyclerView by lazy { findViewById<RecyclerView>(R.id.content_recycler) }
  private val adapter by lazy { ContentAdapter(this) }

  private var contentItems: List<ContentItem>? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    recyclerView.layoutManager = LinearLayoutManager(this)
    recyclerView.adapter = adapter

    if (savedInstanceState != null) {
      contentItems = savedInstanceState.getParcelableArrayList(CONTENT_ITEMS_LIST)
    }

    if (contentItems != null && contentItems?.isNotEmpty() == true) {
      val items = contentItems ?: ArrayList()
      render(items)
    } else {
      presenter.getContentItems()
    }
  }

  override fun render(contentItems: List<ContentItem>) {
    this.contentItems = contentItems
    adapter.clearAndAddContentItems(contentItems)
  }

  override fun onItemClicked(contentItem: ContentItem, titleView: View, subtitleView: View, dateView: View) {
    val navIntent = Intent(this, ContentDetailActivity::class.java)
    navIntent.putExtra(EXTRA_CONTENT_ITEM, contentItem)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      navIntent.putExtra(TRANSITION_TITLE, titleView.transitionName)
      navIntent.putExtra(TRANSITION_SUBTITLE, subtitleView.transitionName)
      navIntent.putExtra(TRANSITION_DATE, dateView.transitionName)
    }
    val titleTransition = createTransitionPair(titleView)
    val subtitleTransition = createTransitionPair(subtitleView)
    val dateTransition = createTransitionPair(dateView)

    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, titleTransition, subtitleTransition, dateTransition)
    startActivity(navIntent, options.toBundle())

  }

  override fun onPause() {
    super.onPause()
    presenter.pause()
  }

  override fun onSaveInstanceState(outState: Bundle?) {
    if(contentItems != null) {
      outState?.putParcelableArrayList(CONTENT_ITEMS_LIST, ArrayList(contentItems))
    }
    super.onSaveInstanceState(outState)
  }

  private fun createTransitionPair(view: View) = android.support.v4.util.Pair(view, ViewCompat.getTransitionName(view))

  companion object {
    private const val CONTENT_ITEMS_LIST = "CONTENT_ITEMS_LIST"
  }
}
