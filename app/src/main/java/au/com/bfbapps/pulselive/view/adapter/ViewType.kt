package au.com.bfbapps.pulselive.view.adapter


interface ViewType {
  fun getViewType(): Int
}