package au.com.bfbapps.pulselive.view

import au.com.bfbapps.pulselive.models.ContentItem


interface ContentDetailView {
  fun render(contentDetail: ContentItem)
}