package au.com.bfbapps.pulselive.view

import android.os.Build
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import au.com.bfbapps.pulselive.R
import au.com.bfbapps.pulselive.data.ContentService
import au.com.bfbapps.pulselive.models.ContentItem
import au.com.bfbapps.pulselive.presenter.ContentDetailPresenter


class ContentDetailActivity : AppCompatActivity(), ContentDetailView {

  private val collapsingToolbar by lazy { findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar) }
  private val detailTitle by lazy { findViewById<TextView>(R.id.detail_title) }
  private val detailAppBarLayout by lazy { findViewById<AppBarLayout>(R.id.detail_app_bar_layout)}
  private val detailImage by lazy { findViewById<ImageView>(R.id.detail_image)}
  private val detailSubtitle by lazy { findViewById<TextView>(R.id.detail_subtitle) }
  private val detailDate by lazy { findViewById<TextView>(R.id.detail_date) }
  private val detailBody by lazy { findViewById<TextView>(R.id.detail_body) }
  private val loadingIndicator by lazy { findViewById<ProgressBar>(R.id.detail_loading) }
  private val presenter by lazy { ContentDetailPresenter(ContentService.create(), this) }

  private var contentItem: ContentItem? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_content_detail)

    val toolbar = findViewById<Toolbar>(R.id.toolbar)
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    var item: ContentItem? = null
    if(savedInstanceState != null) {
      item = savedInstanceState.getParcelable(EXTRA_CONTENT_ITEM)
    }

    if(item == null) {
      item = intent.extras.getParcelable(EXTRA_CONTENT_ITEM)

      presenter.getContentDetail(item.id)
    }

    if (intent != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      detailSubtitle.transitionName = intent.getStringExtra(TRANSITION_SUBTITLE)
      detailDate.transitionName = intent.getStringExtra(TRANSITION_DATE)
      detailTitle.transitionName = intent.getStringExtra(TRANSITION_TITLE)
    }

    addOffsetListener(detailAppBarLayout)

    if(item != null) {
      render(item)
    }

    contentItem = item
  }

  override fun render(contentDetail: ContentItem) {
    contentItem = contentDetail
    detailTitle.text = contentDetail.title
    detailSubtitle.text = contentDetail.subtitle
    detailDate.text = contentDetail.date
    detailImage.setBackgroundResource(selectImage(contentDetail.id))
    if (contentDetail.body == null) {
      loadingIndicator.visibility = VISIBLE
    } else {
      loadingIndicator.visibility = GONE
      detailBody.text = contentDetail.body
    }
  }

  override fun onPause() {
    super.onPause()
    presenter.pause()
  }

  override fun onSaveInstanceState(outState: Bundle?) {
    outState?.putParcelable(EXTRA_CONTENT_ITEM, contentItem)
    super.onSaveInstanceState(outState)
  }

  private fun addOffsetListener(appBarLayout: AppBarLayout) {
    appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
      var isShow = true
      var scrollRange = -1

      override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (scrollRange == -1) {
          scrollRange = detailAppBarLayout.totalScrollRange
        }
        if (scrollRange + verticalOffset == 0) {
          collapsingToolbar.title = contentItem?.title
          isShow = true
        } else if (isShow) {
          collapsingToolbar.title = " "
          isShow = false
        }
      }

    })
  }

  private fun selectImage(id: Int): Int {
    return when(id % 8) {
      0 -> R.mipmap.bg_blue_yellow_jag
      1 -> R.mipmap.bg_bottom_blue_circle
      2 -> R.mipmap.bg_green_jag
      3 -> R.mipmap.bg_light_jag
      4 -> R.mipmap.bg_purple_circle
      5 -> R.mipmap.bg_red_jag
      6 -> R.mipmap.bg_sea_green_jag
      else -> R.mipmap.bg_top_blue_circle
    }
  }

  companion object {
    const val EXTRA_CONTENT_ITEM = "EXTRA_CONTENT_ITEM"
    const val TRANSITION_TITLE = "titleTransitionName"
    const val TRANSITION_SUBTITLE = "subtitleTransitionName"
    const val TRANSITION_DATE = "dateTransitionName"
  }
}