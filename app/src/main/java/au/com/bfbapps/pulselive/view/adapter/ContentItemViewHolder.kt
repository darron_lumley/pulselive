package au.com.bfbapps.pulselive.view.adapter

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.bfbapps.pulselive.R
import au.com.bfbapps.pulselive.models.ContentItem
import kotlinx.android.synthetic.main.content_list_item.view.*


class ContentItemViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.content_list_item, parent, false)) {

  private val title = itemView.title
  private val subtitle = itemView.subtitle
  private val date = itemView.date

  fun bind(item: ContentItem, onItemClickedListener: OnItemClickedListener) {
    title.text = item.title
    subtitle.text = item.subtitle
    date.text = item.date

    ViewCompat.setTransitionName(subtitle, item.subtitle)
    ViewCompat.setTransitionName(date, item.date)
    ViewCompat.setTransitionName(title, item.title)

    super.itemView.setOnClickListener { onItemClickedListener.onItemClicked(item, title, subtitle, date) }
  }
}