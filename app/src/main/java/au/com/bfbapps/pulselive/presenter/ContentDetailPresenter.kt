package au.com.bfbapps.pulselive.presenter

import au.com.bfbapps.pulselive.data.ContentService
import au.com.bfbapps.pulselive.view.ContentDetailView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ContentDetailPresenter(contentService: ContentService, view: ContentDetailView):
    BasePresenter<ContentDetailView>(contentService, view) {

  fun getContentDetail(id: Int) {
    disposable = contentService.getContentItem(id)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .map { it.item }
        .subscribe { view.render(it) }
  }

}