package au.com.bfbapps.pulselive.models

import android.annotation.SuppressLint
import android.os.Parcelable
import au.com.bfbapps.pulselive.view.adapter.AdapterItems
import au.com.bfbapps.pulselive.view.adapter.ViewType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@SuppressLint("ParcelCreator") // Parcelize annotation creates the CREATOR automatically
@Parcelize
data class ContentItem(val id: Int, val title: String, val subtitle: String, val body: String? = null, val date: String): ViewType, Parcelable {
  override fun getViewType() = AdapterItems.CONTENT
}

data class ContentList(@SerializedName("items")val contentItems: List<ContentItem>)

data class ContentDetail(val item: ContentItem)