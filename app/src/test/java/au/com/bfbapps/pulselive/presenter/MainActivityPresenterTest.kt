package au.com.bfbapps.pulselive.presenter

import au.com.bfbapps.pulselive.RxImmediateSchedulerRule
import au.com.bfbapps.pulselive.data.ContentService
import au.com.bfbapps.pulselive.models.ContentItem
import au.com.bfbapps.pulselive.models.ContentList
import au.com.bfbapps.pulselive.view.MainActivityView
import io.reactivex.Observable
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito


class MainActivityPresenterTest {

  companion object {
    @ClassRule
    @JvmField
    val schedulers = RxImmediateSchedulerRule()
  }

  @Test
  fun shouldRenderOnView() {
    val contentService = Mockito.mock(ContentService::class.java)
    val view = Mockito.mock(MainActivityView::class.java)
    val presenter = MainActivityPresenter(contentService, view)

    val returnedItem = ContentList(arrayListOf(
        ContentItem(1, "title1", "subtitle1", "body1", "01/01/1970")))


    Mockito.`when`(contentService.getContentItemsList()).thenReturn(Observable.just(returnedItem))
    presenter.getContentItems()
    Mockito.verify(view).render(returnedItem.contentItems)
  }

}