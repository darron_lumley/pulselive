package au.com.bfbapps.pulselive.presenter

import au.com.bfbapps.pulselive.RxImmediateSchedulerRule
import au.com.bfbapps.pulselive.data.ContentService
import au.com.bfbapps.pulselive.models.ContentDetail
import au.com.bfbapps.pulselive.models.ContentItem
import au.com.bfbapps.pulselive.view.ContentDetailView
import io.reactivex.Observable
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito.*



class ContentDetailPresenterTest {

  companion object {
    @ClassRule @JvmField
    val schedulers = RxImmediateSchedulerRule()
  }

  @Test
  fun shouldRenderOnView() {
    val contentService = mock(ContentService::class.java)
    val view = mock(ContentDetailView::class.java)
    val presenter = ContentDetailPresenter(contentService, view)

    val returnedItem = ContentDetail(
        ContentItem(1, "title1", "subtitle1", "body1", "01/01/1970"))


    `when`(contentService.getContentItem(1)).thenReturn(Observable.just(returnedItem))
    presenter.getContentDetail(1)
    verify(view).render(returnedItem.item)
  }

}